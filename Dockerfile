FROM node:alpine

WORKDIR /usr/app
COPY ./ /usr/app
RUN npm install

COPY packages/server/.env-example packages/server/.env
CMD [ "npm","start" ]